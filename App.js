/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useRef} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Modalize} from 'react-native-modalize';

const App: () => React$Node = () => {
  const modalizeRef = useRef<Modalize>(null);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  return (
    <>
      <TouchableOpacity onPress={onOpen}>
        <Text>fdfdf</Text>
      </TouchableOpacity>

      <Modalize ref={modalizeRef} withReactModal>
        <View>
          {/* <ScrollView> */}
          {[1, 2, 3, 4].map((item) => (
            <View>
              <Text>{item}</Text>
            </View>
          ))}
          {/* </ScrollView> */}
        </View>
      </Modalize>
    </>
  );
};

export default App;
